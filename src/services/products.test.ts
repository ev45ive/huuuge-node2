import sinon from "sinon";
import { Product, ProductsService } from "./Products";

interface Repository<T> {
  getAll(): T[]
}

const mockProducts = [
  { id: '123', name: "Prod 123" },
  { id: '234', name: "Prod 234" },
  { id: '345', name: "Prod 345" },
]

describe.only('Products Service', () => {
  let service: ProductsService
  let repo: Repository<Product>

  beforeEach(() => {
    repo.getAll = sinon.fake()
    service = new ProductsService()
  })

  it('is created', () => {
    expect(service).to.exist
  });

  it('gets products list', async () => {

    await expect(service.getProducts()).to.eventually.eq(mockProducts)

    // const result = await service.getProducts()
    // expect(result).to.equal(mockProducts)

    // return service.getProducts().then(()=>{
    //   expect()
    // })

    // expect(service.getProducts()).to.equal(mockProducts)
    // expect(service.getProducts()).to.deep.equal(mockProducts)
  })

  it('gets one product', () => {
    expect(service.getProductById('123')).to.deep.equal(mockProducts[0])
  })

  it('creates product ', () => {
    // expect(service.getProducts()).to.not.include({name:'Prod 123'})
    const createdProduct = service.createProduct({
      name: 'Prod 555'
    });
    expect(createdProduct).to.haveOwnProperty('id')
    expect(service.getProducts()).to.include(createdProduct)
    expect(service.getProductById(createdProduct.id)).to.be.eq(createdProduct)

  })
  it('updates product ')
  it('deletes product ')

});