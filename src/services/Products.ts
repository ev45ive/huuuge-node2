export interface Product {
  id: string,
  name: string
}
const mockProducts: Product[] = [
  { id: '123', name: "Prod 123" },
  { id: '234', name: "Prod 234" },
  { id: '345', name: "Prod 345" },
]

export class ProductsService {

  createProduct(product: { name: string }) {
    const id = this.getNextId()
    const newProduct = {
      id, ...product
    }
    mockProducts.push(newProduct)
    return newProduct;
  }

  getNextId() {
    return '555'
  }

  getProductById(id: string) {
    return mockProducts.find(p => p.id == id)
  }

  async getProducts() {
    return mockProducts
  }
}
