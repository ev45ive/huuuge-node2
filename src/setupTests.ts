
import { expect } from 'chai'
import chai from "chai"
import sinon from "sinon"
import sinonChai from "sinon-chai"

import chaiAsPromised from "chai-as-promised"

chai.use(chaiAsPromised);
chai.use(sinonChai);

declare global {
  var expect: Chai.ExpectStatic
}

global.expect = chai.expect
