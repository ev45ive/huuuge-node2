import { productsRoutes } from "./products.routes";
import express, { Express } from 'express'
import supertest from 'supertest'
import { getProductsService } from "../services";
import { ProductsService } from "../services/Products";
import sinon from "sinon";


describe.only('Products Routes', () => {
  let app: Express,
    client: supertest.SuperTest<supertest.Test>,
    service: ProductsService

  before(() => {
    service = getProductsService()
    // sinon.mock(service)
    const spy = sinon.stub(service,'getProducts')

    app = express()
    app.use('/products', productsRoutes)
    client = supertest(app)
  })

  it('serve products', (done) => {
    client
      .get("/products/")
      .expect(200)
      // .expect((res)=>{ expect(res.body)...}
      .expect(()=>{
        expect(service.getProducts).to.have.been.called
      })
      .expect([])
      .end(done)
  });

  it.skip('gets one product', () => {
    return client
      .get("/products/" + 123)
      .expect(200)
      .expect({ id: '123' })
  });


  it.skip('gets one product', async () => {
    // await client
    return client
      .get("/products/" + 123)
      .expect(200)
      .expect({ id: '123' })
  });



});