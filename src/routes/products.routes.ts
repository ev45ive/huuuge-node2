import Router from "express-promise-router";
import { getProductsService } from "../services";

export const productsRoutes = Router()

productsRoutes.get('/', async (req, res) => {

  const products = getProductsService()

  // throw new Error('upsss..')

  const results = await products.getProducts();

  res.send(results)

  // return 'next'
  // return 'route'
})

  // .get('/', async (req: Request, res: Response, next) => {
  //   // try{
  //     const products = getProductsService()

  //     throw new Error('upsss..')

  //     const results = await products.getProducts();

  //     res.send(results)
  //   // }catch(err){
  //   //   next(err)
  //   // }
  // })


  // process.on('unhandledRejection', (error:any) => {
  //   // Will print "unhandledRejection err is not defined"
  //   console.log('unhandledRejection', error.message);
  // });