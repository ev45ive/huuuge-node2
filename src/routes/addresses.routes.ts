
import { Router } from 'express';

const addresses = [
    {
        street: 'Długa 3',
        city: 'Bydgoszcz',
        postcode: '85-034'
    }
]

export const addressRoutes = Router()

addressRoutes.get('/', (req, res) => {
    res.send(addresses)
})