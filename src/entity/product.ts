import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Category } from "./category";

@Entity()
export class Product {

  @PrimaryGeneratedColumn()
  id?: number

  @Column()
  name: string = ''

  @Column()
  price: number = 0

  @ManyToOne(type => Category, category => category.products)
  category?: Category
}
